# Terraform course from Udemy
## DevOps Deployment Automation with Terraform, AWS and Docker


Local directory `/Users/simonhewitt/ComputerStuff/devops/terraform/Udemy`.    

[udemy](https://www.udemy.com/course/devops-deployment-automation-terraform-aws-docker/learn/lecture/19270964?components=deal_badge%2Cdiscount_expiration%2Cgift_this_course%2Cprice_text%2Cpurchase%2Credeem_coupon%2Ccacheable_deal_badge%2Ccacheable_discount_expiration%2Ccacheable_price_text%2Ccacheable_buy_button%2Cbuy_button%2Cbuy_for_team%2Ccacheable_purchase_text%2Ccacheable_add_to_cart%2Cmoney_back_guarantee%2Cinstructor_links%2Ctop_companies_notice_context%2Ccurated_for_ufb_notice%2Csidebar_container%2Cpurchase_tabs_context#overview). £10.99, 1/1/21.

Mark Winterbottom

**Repos**

Under the root `/Users/simonhewitt/ComputerStuff/devops/terraform/Udemy` 
there are two directories `recipe-app-api-devops`  and `recipe-app-api-proxy`.
Each has a git repo.

recipe-app-api-devops in `https://gitlab.com/simon.hewitt/recipe-app-api-devops` and 
-proxy in `https://gitlab.com/simon.hewitt/recipe-app-api-proxy`. 

The -proxy just exists to build and push an Nginx proxy, most of the signofocant work 
is in
-devops. 

This course is huge in scope, but gallops through it very swiftly. The Git repos shoiuld be a useful resource.

And I finished it!: ![certificate](./resources/udemy-aws-course-completion.png)


Getting help: 

Techniques:
1. Write the problem down
2. Google it!
3. Ask others - Udmy Q&A. 

## AWS Components and other technologies

WOW:

* IAM Identity and Access Management
* ECS Elastic Container Service
    *  ECS FarGate (Serverless)
* EC2 - Admin server, single point of entry - bastion host?
* VPC Virtual Private Cloud. Isolate an Infra 'set'
* RDS - 
* ALB Application LoadBalancer
    * Zero downtime updates
* S3 (As FarGate is stateless). Cheaper than ?? disk
* cloudwatch
* AWS Certtificate Manager - ACM.
* ECR Amazon Elastic Container Registry

__TerraForm__ as multiplatform

__GitLab__ for CI/CD and more
* Is Open Source, so can easily self host

__Docker__

__Django REST Framework__

### Tools:

* VSC - Code. OK.
    * With Docker and Terraform (Anton Kulikov)
* Docker - yes.
* Brew - yes
* AWS Vault
* GIT
* Chrome Modheader - done.

### AWS setup and use

User `mcl-main-user`. Login [here](https://signin.aws.amazon.com/oauth?redirect_uri=https%3A%2F%2Fconsole.aws.amazon.com%2Fconsole%2Fhome%3Fstate%3DhashArgs%2523%26isauthcode%3Dtrue&client_id=arn%3Aaws%3Aiam%3A%3A015428540659%3Auser%2Fhomepage&response_type=code&iam_user=true&account=524430743157&code_challenge=ER96oOJOJkhmbXAHXC4KyU-3ky7BYVTAOC9NH6zg63k&code_challenge_method=SHA-256) with:

	Account ID 524430743157
	Username: mcl-main-user
	Password in SplashID
	New Access Key: AKIAXUGUCCZ2S77QXTRN
	And new secret access key in SplashID
	
1. Create policy ForceMFA, based on AWS best practice 
   [here](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_examples_aws_my-sec-creds-self-manage.html). 
   Done from
   [console](https://console.aws.amazon.com/iam/home?region=eu-west-2#/policies)
2. New user `simon.udemy` Then set up MFA (using same Yubikey), using account security credentials page (The course is wrong on this one).
     1. Best practice - DON'T download the access key CSV file
3. __AWS-VAULT__ `$aws-vault add simon.udemy`, it asks for IAM key and secret key for this user, 
   and then a aws-vault password - in splash-id under aws-vault.
   To refresh, run `$ unset AWS_VAULT` then run alias `awsv`, then enter Authenticator number (on iPhone Google Authenticator, 6-digit number) and password.
4. Setting up MFA for CLI/API access using AWS-VAULT is complex, 
   see [here](https://www.udemy.com/course/devops-deployment-automation-terraform-aws-docker/learn/lecture/19173784#overview). Works well, but use Google Authenticator (NOT Yubikey, which is even more complex)
5. To use: `aws-vault exec simon.udemy --duration=12h` then asks for Google Authenticator token. Then asks for AWS-VAULT password (Keyboard Maestro ^p v)

To use AWS Vault:

		aws-vault exec simon.udemy --duration=12h'
		(alias awsv)
		1st Google Authenticator token for AWS simon.udemy, on iPhone
		2nd password from SplashID (or KeyboardMaestro ^P-V)
		Creates 3 env vars AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_SESSION_TOKEN


## Development

The Recipe Django REST App source is in 
`/Users/simonhewitt/ComputerStuff/devops/terraform/Udemy/recipe-app-api-devops-starting-code`, or clone from [GitLab](https://gitlab.com/LondonAppDev/recipe-app-api-devops-starting-code).




## Notes

* IAC Infrastructure as code
* GIT (GitLab) - when cloning, use the SSH variant else the SSH keys never work!
* 


**Selecting output from a CLI command**    
One - JQ, eg:

	aws iam list-users|jq '.Users[].UserName'

See JQ at [JQ](https://stedolan.github.io/jq/manual/#Basicfilters)

Two - no dependencies, use AWS --query. This uses [JMESPath](https://jmespath.org/).  
eg" `aws iam list-users --query 'Users[*].UserName'` returns

	[
    "mcl-main-user",
    "simon.udemy"
	]

Probbaly very powerful, bit more difficult. See AWS docs [here](https://docs.aws.amazon.com/cli/latest/userguide/cli-usage-output.html#cli-usage-output-filter)

More examples:

	$ aws iam list-users --query 'Users[*].{name:UserName, arn:Arn}'       
	[
   	 {
   	     "name": "mcl-main-user",
   	     "arn": "arn:aws:iam::524430743157:user/mcl-main-user"
   	 },
   	 {
   	     "name": "simon.udemy",
   	     "arn": "arn:aws:iam::524430743157:user/simon.udemy"
   	 }
	]


## GitLab CI

![GitLab Work Flow](./resources/gitlab-workflow.png)


## The recipe project

### NGINX Proxy

New project in GitLAb `recipe-add-api-proxy` [here](https://gitlab.com/simon.hewitt/recipe-add-api-proxy)

Steps:  

1. Make pipeline logs private (members of the project) as these can leak cred etc.  [here](https://gitlab.com/simon.hewitt/recipe-add-api-proxy/edit). DONT FORGET TO SAVE! Also [here](https://gitlab.com/simon.hewitt/recipe-add-api-proxy/-/settings/ci_cd) `general Pipelines`, untick `Public pipelines`.
2. `Protected Branches` All quite complex, see [Udemy](https://www.udemy.com/course/devops-deployment-automation-terraform-aws-docker/learn/lecture/19173852#overview). Protect any branch with name e
3. Set up NGINX to serve static and Django, see `default.conf.tpl`
	4. Note `location /static` is the value defined in Django `settings.py`. `alias` maps it to a static volume (`/vol/static`)  


#### Code

			location / {
        		uwsgi_pass ${APP_HOST}:${APP_PORT};
    		}

### Entrypoint.sh

This runs at the start of the Docker. It populates the template and starts Docker.  

`envsubst` new one for me - substitutes `${env-var}` with the value of `env-var`.  

See `entrypoint.sh`

`Dockerfile` use `nginxinc/nginx-unprivileged` (rather than common NGINX image), as this runs NGINX Without root.


**NOTE** 
* `$ set -e` causes exit with fail if any command fails.
* `Dockerfile` does a touch and chown so the target conf file exists in the correct ownership.

### AWS ECR

1. Is a Docker repo [here](https://eu-west-2.console.aws.amazon.com/ecr/repositories/private/524430743157/recipe-add-api-proxy?region=eu-west-2). Named `recipe-add-api-proxy
`

2. IAM user just to push to this repo. Policy `recipe-add-api-proxy-CIPushECR `, added to new user `recipe-add-api-proxy-CI`. This user cannot log into any console.

**Notes on GitLab user:**
Username `recipe-add-api-proxy-CI` is used by GitLab CI. In the project, add varibles for this user creds `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` (spell exactly as this). Key is `AKIAXUGUCCZ2WDOVPHG7`, secret is only in gitlab now. Also `ECR_REPO`, the URI for the AWS ECR just created. (URI not ARN here, ??)

Click `protect variable`, means it can only be used in the 'special' branches `*-release` and `mask variable` so it is not shown.



## Resources

* [AWS FarGate cost estimator](http://fargate-pricing-calculator.site.s3-website-us-east-1.amazonaws.com/). This course AWS will cost $0.40 per Hour, $9 per Day for 3 environments, just take care. May be deploying 3 at once! - Only $5.89/Day for 2 - Prod and Staging. See [here](https://docs.google.com/spreadsheets/d/1VNup01_bFZRSKzra6FSo-WDDM1ZzL2IRmuq99NZ5WgY/edit#gid=0).
* 


## terraform setup

**S3 bucket**

`com.mollycode.recipe-app-api-devops-tfstate` for terraform state.  
**Dynamo DB** Table named `recipe-app-api-devops-tf-state-lock` primary key `LockID`.  
Used to lock terraform so only One running at a time. Global lock.   
Plus new ECR Repo `recipe-app-api-devops`. URI is `524430743157.dkr.ecr.eu-west-2.amazonaws.com/recipe-app-api-devops` 

**ADVICE** Lock down AWS Provider version, by: 

	provider "aws" {
   	 	region = "eu-west-2"
     	version = "~> 2.54.0"
	}
	
**ADVICE** Don't install Terraform locally (or don't use it). A later version of TF may (will) upgrade the TF State file , makingit unuseable to other users. So build TF in docker-compose. 

See `deploy/docker-compose/yml`. 
First run awsv (_qv_) to set up AWS Creds into 3 env vars.  
Then run `docker-compose -f deploy/docker-compose.yml run --rm terraform init` to set up terraform with the S3 bucket etc.  

In general, do `docker-compose -f deploy/docker-compose.yml run --rm terraform <command>`
e.g. `fmt` etc to run TF commands

Lesson [udemy 40](https://www.udemy.com/course/devops-deployment-automation-terraform-aws-docker/learn/lecture/19173966#overview)

**Key terraform commands**:  

* init
* fmt
* validate
* plan
* apply
* show
* destroy


### EC2 Bastion Host

Lesson [41](https://www.udemy.com/course/devops-deployment-automation-terraform-aws-docker/learn/lecture/19173970#overview)

Uses TF to create it, `/deploy/bastion.tf`.
To get the ARN, fiddly multi-step process. In the `.tf`, 

1. see the `data` block
2. get the AMI current name, by a) start EC2 creation in Amazon console, and select the Amazon Linux AMI name listed (usually first),
3. b) back to ERC2 console and select 'AMIs', search for this AMI 'nickname'
4. Get the full name eg `amzn2-ami-hvm-2.0.20201218.1-x86_64-gp2`
5. Paste into `values = [""], but wildcard as: amzn2-ami-hvm-2.0.*-x86_64-gp2`
6. This gets always the LATEST ARN for that type of AMI.


**USEFUL NOTE** In Terraform docs [here](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami), type `aws_instance` into search box.This will show two groups:

* Data Sources: Use these to find out about MY EC2 instances, already existing
* Resources - Used this to Create an EC2 instance

## Terraform WorkSpaces

Alias `runtf` runs the docker-compose terraform container

`runtf workspace list` - Lists them! Current active has `*`
`runtf workspace new dev` -New workspace

Created new file `variables.tf`. Available as `${var.name}`

Then `locals` eg:

		locals {
 			 prefix = ${var.prefix}-${terraform.workspace}  # raad-dev 
		}

and then THESE are available as `${local.xxxx}`. Note confusing plural locals / singular `local` use.

technique for common tags:

		tags = merge (
      	local.common_tags,
      	map("Name", "${local.prefix}-bastion")
  		)

## Lesson 45 - GitLab CI for terraform

**Environment Branches** 

1. Master --> deployed to Staging
2. If OK, deploy to Prod

**NOTE** terraform image usually runs a terraform command ONLY. To run Unix commands, add image and entrypoint, from [here](https://gitlab.com/LondonAppDev/recipe-app-api-devops-course-material/snippets/1944969) into the `.gitlab-ci.yml`:

		image:
  			name: hashicorp/terraform:0.12.21
  			entrypoint:
    			- '/usr/bin/env'
    			- 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

Use: `    - terraform init -backend=false` means do not set up AWS backend, so does not needs AWS Creds.   
`- terraform fmt -check` fails if formatting not correct

**IAM User**  
CreateIAM user that only deploys terraform. Policy ??? allows role `recipe-app-api-devops-ci` to do terraform stuff on AWS. KEY is  `AKIAXUGUCCZ22TF5F5EL` , secret key is in CI secrets, as `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`, both protected and masked. And `ECR_REPO` is the container.

**NOTE** This project commits using CI_COMMIT_SHORT_SHA as the tag version, whereas the other project (???) uses verions from commit tags.

**My speedup**  
I have sped up the biold a little by creating a new stage `Build Tools` that builds an image 
`  builder_tool_image: "registry.gitlab.com/simon.hewitt/recipe-app-api-devops/recipe_app_api_devops_tools:latest"` 
that is in the GitLab container registry, while the App images are in the AWS container registry. It speeds up `Test and Lint` but only by about 40 seconds.

The overall build could be sped up by:

1. Only build the App once. Push as `:under_test`
2. Use Docker (not compose) to build it, as:
3. Pull and use docker-cache
4. change docker-compose.yml to use the image instead og `build .`.
5. The final push then does not need to build again, just retag as `:latest` and `:commit-short-sha`
6. Actually by using CI `.service` twice, may be possible to store both images in AWS?

More work to check this all out.

## VPC, subnets and gateways

Each environment will have its own VPC.  
`cidr` blocks: /16 gives 65536 addresses. /28 gives 16. The `/xx/` is the number of bits definedd in the subnet mask, so /16 is 255.255.0.0 => 2^16 available subnet addresses. \24 is 255.255.255.0 hence 254 address (2^8). Smallest possible is /28, 32-28=4, 2^4 =16 addresses (11 useable).

**NOTE TAGS**  
* Use: `  tags = merge(local.common_tags, map("Name", "${local.prefix}-main"))`  to include common tags in all reources.
* Using terraform in a docker container: `docker-compose -f docker-compose.yml run --rm terraform validate`

### network.tf

Take a look at `deploy/network.tf`. 
This:  
1. Creates a new VPM "main" `aws_vpc.main`
2. Adds an internet gateway `aws_internet_gateway.main`
3. Creates subnet A with:
    1. Subnet: `aws_subnet.public_a` for addresses "10.1.1.0/24", in Zone a
    1. Route table `aws_route_table.public_a`. This is initially empty
    1. Associate the new route table with the new subnet: `aws_route_table_association.public_a`
    1. Create a route table entry `aws_route.public_internet_access_a` saying "use the internet gateway to reach any address"
    1. Create an elastic IP address `aws_eip.public_a`
    1. Create a NAT gateway to the net EIP (above) from this subnet `aws_nat_gateway.public_a`
1. Repeat for `_b`

## Database PostGres

see `database.tf`. Fairly standard, except:

**NOTE TFVARS**   
Files `.tfvars` are picked up by terraform to supply values as defined in `variables.tf` (if no default supplied? Or does this
override?) . Note `.tfvars` is listed in `.gitignore` so not saved, good for doing a local build. 
For a CI build, use GitLab CI values as usual. Here the convention is that CI values `TF_VAR_env-var-name`
is created as an env var with `env-var-name` eg `TF_VAR_db_username`

### Deploying the bastion host

1. Created a user-data.sh script that installs and runs docker into the bastion.
1. Added this to bastion.tf so it runs on creation.
1.  Added loads of access privileges to policy `recipeAppApi-CI `  (Looks a bit dangerous to me)
1.   Added existing SHA key to EC2

`instance profile` assigns a profile to an EC2 (not to a user). The roles is created in TF, in `

The linkage is: (an `EC2` has) -> (`aws_iam_instance_profile`, which has one or more) -> (`aws_iam_role`,
 which has one or more  policies, each attached by a `aws_iam_role_policy_attachment`.  
I think.

**And Finally:**

`$ ssh ec2-user@ec2-18-130-153-232.eu-west-2.compute.amazonaws.com` **ALL WORKS.**

## ECS - Elastic Cluster Service

**Lesson 72 up**

**NOTE** Don't forget `locals.common_tags` and the way to merge and map additional
values into this. Very seful.

**ECS** Define **TWO** roles   
1. Permissions needed to start the ECS, in `deploy/templates/ecs/task-exec-role.json`
1. The permissions that EC2 needs to do its job, in `deploy/templates/ecs/assume-role-policy.json`

See `deploy/ecs.tf`, quite complicated? - has useful comments, qv.  
AWS [docs](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html#container_definitions)
on container definitions.  
Terraform [docs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_task_definition#example-usage)

Interestingly each container contains a Django AND a NGINX Proxy. Makes NGINX simpler, as it is running on the same node, so 
just accesses it with 127.0.0.1:9000. NGINX can do load sharing itself of course.

**More Notes**  

1. Local variables - To make it easy to run locally, put stuff in `deploy/local.tfvars`. Add this to `.gitignore`. 
Run as: 
   
        docker-compose -f ./deploy/docker-compose.yml run --rm terraform plan -var-file="local.tfvars"
    But repeat, don't allow it to be committed.

1. Terraform will 'post' env vars in its run environment (??) from CI if saved as 
   eg `TF_VAR_db_password` => db_password. Not sure of the complete train of links here.
   

## lesson 81 - Using the Bastion

get the bastion name from the pipeline o from the AWS EC2 console. Then connect with ` ssh ec2-user@ec2-18-135-97-74.eu-west-2.compute.amazonaws.com`.
Remember the `ec2-user` bit.

The log into AWS ecr (Container registry) with   
`$(aws ecr get-login --no-include-email --region eu-west-2)`

Run a command to create a Django superuser on the DJ docker image:

        docker run -it \
           -e DB_HOST=<DB_HOST> \
           -e DB_NAME=recipe \
           -e DB_USER=recipeapp \
           -e DB_PASS=<DB_PASS> \
           <ECR_REPO>:latest \
           sh -c "python manage.py wait_for_db && python manage.py createsuperuser"

DB Host also in pipeline output.  
DB_PASS can be found in GitLab settings/CI/values.  
ECR_REPO can be found in AWS container service (ECR) console, `recipe-app-api-devops`, the URL

The final working command is:  
`docker run -it  -e DB_HOST=raad-staging-db.cpx6afsdwolz.eu-west-2.rds.amazonaws.com  -e DB_NAME=recipe  -e DB_USER=recipeapp  -e DB_PASS=grockle-dog-lemon  524430743157.dkr.ecr.eu-west-2.amazonaws.com/recipe-app-api-devops:latest  sh -c "python manage.py wait_for_db && python manage.py createsuperuser"`

This creates a superuser, with my email simon.d.hewitt@gmail.com 
and password `dogsbreath-for-99`.  

**WHAT is interesting** here is that we do not connect to an existing Django 
EC2, but rather run up a whole new Docker container. ASs it shares the same DB, 
the new superuser details are in the DB and can be used anywhere.

In doing this, I found errors in my bastion `user-data.sh` - which raises, how do we test as we go?
Fixed now and will be corrected next time the bastion restarts.

NEXT:  

**Public access to the Django app!**  
Get the public address from ` Clusters raad-staging-cluster `, any task and find the public IP:  

        Network
            Network mode
            awsvpc
            ENI Id
            eni-0833e0a55aaae6e3f
            Subnet Id
            subnet-032e7e1c436c8a1bb
            Private IP
            10.1.1.171
            Public IP
            35.178.249.13
            Mac address
            06:c2:95:0b:26:08

In a browser, open `http://35.178.249.13:8000/admin/` and use the Superuser just created.
IT WORKS! fuck me.

## Lesson 84 - Load balancer

1st update IAM -> Policies -> PoliciesrecipeAppApi-CI  
(Remember click `filter policies` -> `user managed` to easily find it).
Add: `"elasticloadbalancing:*"`


## Lesson 87 onwards

Store uploaded media into S3.


Google mod-header - use (in stead of postman for instance) to add an auth header to 'simulate'
an API call. [here](https://www.udemy.com/course/devops-deployment-automation-terraform-aws-docker/learn/lecture/19174242#notes)

## Lesson 92 and up - DNS and HTTPS

[here](https://www.udemy.com/course/devops-deployment-automation-terraform-aws-docker/learn/lecture/19174248#notes)  

Interesting:

1. Register a custom DNS Name
1. Create an HTTPS cert for it
1. Crdeate a DNS entry

**! DNS**  

DNS Name in Route53

registered `simon-d-hewitt.co.uk` for $10, disabled auto-renew. Nore Route53 domain also costs $$.
Add to policy `arn:aws:iam::524430743157:policy/recipeAppApi-CI` 
to enable `acm` (*Certificate Management*) and route53 access.  
**NOTE** Don't use terraform to register the domain, as they require annual renewal etc. tf (may?) be able 
to do it, but best not.

**NOTE** Common subdomain naming pattern:

        variable "subdomain" {
          description = "Subdomain per environment"
          type = map(string)
          default = {
            production = "api"
            staging = "api.staging"
            dev = "api.dev"
          }
        }

**NOTE** Remember terraform can se map and decode to calculate variable values.

### 2. get a certificate

In dns.tf: `resource "aws_acm_certificate" "cert"`

Purpose of a cert is to validate that I own the DNS Name (uri). 
This is: `validation_method = "DNS"`  
I don't fully understand it.

    Dear AWS customer,

    We successfully registered the simon-d-hewitt.co.uk domain. We also created a hosted zone for the domain.
    
    Your next step is to add records to the hosted zone. These records contain information about how to route traffic for your domain and any subdomains. To learn how to add records to your hosted zone, see Working with Records.
    
    If you did not request this change, contact Amazon Web Services Customer Support immediately.
    
    Regards,
    Amazon Route 53 


## 3 Update Load Balancer with the new FQDN.

**Remember** `  protocol = "HTTPS"` is case sensitive - UC.




# Visualisation Tools

**Hava**  
https://app.hava.io/simon.d.hewitt@gmail.com/environments?view=infrastructure  
Flaky. No.

**Cloudcraft**  
https://app.cloudcraft.co/blueprint/889ab1f8-7619-4b6f-85af-93f337b19a72  
Looks good, but import is paid only, $49/month, too much. 14 day trial?

