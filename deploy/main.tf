terraform {
  backend "s3" {
    bucket         = "com.mollycode.recipe-app-api-devops-tfstate"
    key            = "recipe-app.tfstate"
    region         = "eu-west-2"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }

}

provider "aws" {
  region  = "eu-west-2"
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}" // e.g. : raad-dev 
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
    Use         = "Udemy"
  }
}

data "aws_region" "current" {}