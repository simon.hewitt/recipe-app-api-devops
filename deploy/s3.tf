// S3 bucket to host media files

resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-files"
  acl           = "public-read"
  force_destroy = true // Allows destroy without "Are you sure?" step - enables terraform destroy
}

