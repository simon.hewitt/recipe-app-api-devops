#!/bin/bash
# Runs when the Bastion EC2 created, to start Docker

sudo yum update yum
sudo yum update
sudo amazon-linux-extras install -y docker
sudo systemctl enable docker.service
sudo systemctl start docker.service
sudo usermod -aG docker ec2-user
