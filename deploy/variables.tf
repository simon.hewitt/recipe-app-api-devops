variable "prefix" {
  default = "raad" # Recipe App Api Devops
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "simon.d.hewitt@gmail.com"
}

variable "bastion_instance_type" {
  type        = string
  description = "Instance type eg t2.micro"
  default     = "t2.micro"
}

variable "db_username" {
  # fetched from GitLab-CI values or tf-vars file (for local use)
  description = "Username for RDS Postgres DB"
}

variable "db_password" {
  description = "Password for RDS Postgres DB"
}

variable "bastion_key_name" {
  default     = "recipe-app-devops-bastion"
  description = "Name of the key pair associated with EC2s"
}

variable "ecr_image_api" {
  // The Docker container, from the AWS ECR - the registry.
  // CI should override with the :commit SHA, this provides a backstop
  description = "ECR Image for API"
  default     = "524430743157.dkr.ecr.eu-west-2.amazonaws.com/recipe-app-api-devops:latest"
}


variable "ecr_image_proxy" {
  description = "ECR Image for Proxy"
  default     = "524430743157.dkr.ecr.eu-west-2.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Django secret key"
  // No default
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "simon-d-hewitt.co.uk"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}

