resource "aws_db_subnet_group" "main" {
  name = "${local.prefix}-main"
  subnet_ids = [
    aws_subnet.private_a.id,
    aws_subnet.private_b.id,
  ]
  tags = merge(local.common_tags, map("Name", "${local.prefix}-main"))
}

resource "aws_security_group" "rds" {
  description = "Allow inbound access to the RDS instance"
  name        = "${local.prefix}-rds-inbound-access"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432
    security_groups = [
      aws_security_group.bastion.id,
      aws_security_group.ecs_service.id
    ]
  }

  tags = merge(local.common_tags, map("Name", "${local.prefix}-main"))
}


resource "aws_db_instance" "main" {
  identifier        = "${local.prefix}-db"
  name              = "recipe"
  allocated_storage = 20
  # GB
  storage_type         = "gp2"
  engine               = "postgres"
  engine_version       = "11.4"
  instance_class       = "db.t2.micro"
  db_subnet_group_name = aws_db_subnet_group.main.name
  password             = var.db_password
  username             = var.db_username
  multi_az             = false
  # True for real prod.
  backup_retention_period = 0
  # Retention cost $$
  skip_final_snapshot = true
  # Does not work well with terraform, repeated destroys try and fail to use the same snapshot name
  vpc_security_group_ids = [
  aws_security_group.rds.id]
  tags = merge(local.common_tags, map("Name", "${local.prefix}-main"))
}
